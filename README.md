# Project and specialization course

You need: `python3.6` to run this project. 
`pip install -r requirements.txt` should fix most requirements. 
Then, download the datasets:
`wget http://files.grouplens.org/datasets/movielens/ml-latest-small.zip && unzip ml-latest-small.zip`
`wget http://files.grouplens.org/datasets/movielens/ml-20m.zip && unzip ml-20m.zip`